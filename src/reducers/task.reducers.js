import {TASK_INPUT_CHANGE_HANDLER, TASK_FILTER_BUTTON_HANDLER} from "../constants/task.constants";
import dataDrink from "../assets/data/data";

const initialState = {
    input: "",
    taskList: "",
    status: false,
    id: 0
}

const taskReducer = (state = initialState, action) =>{
    switch (action.type) {
        case TASK_INPUT_CHANGE_HANDLER:
            state.input = action.payload;
            break;
        case TASK_FILTER_BUTTON_HANDLER:
            if(state.input === ""){
                state.status = false;
                break;
            }
            
            var result = dataDrink.filter( ({name}) => name.toLowerCase() === state.input.toLowerCase());
            if(result.length === 0){
                state.status = false;
                break;
            }
            result.map((value, index) => {
                state.taskList = value.name;
                state.id = index + 1;
                state.status = true;
            });
            break;
            // console.log(state.taskList);
            // console.log(state.id)
        default:
            break;
   }
    return { ...state };
}

export default taskReducer;