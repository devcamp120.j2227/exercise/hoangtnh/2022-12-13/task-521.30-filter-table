import { Container } from "@mui/system"
import { Button, Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField  } from "@mui/material";
import Drink from "../assets/data/data";
import Paper from '@mui/material/Paper';
import { useDispatch, useSelector } from "react-redux";
import { inputChangeHandler, buttonFilterHandler} from "../actions/task.action"
const Task = () =>{
    // B3: Khai báo dispatch để đẩy action tới reducer
    const dispatch = useDispatch();

    // B1: Nhận giá trị khởi tạo của state trong giai đoạn mounting
    const { input, taskList, status, id } = useSelector((reduxData) => {
        return reduxData.taskReducer;
    })
    const onButtonFilterHandler = () =>{
        console.log("Lọc thông tin")
        dispatch(buttonFilterHandler());
        console.log({input});
    }
    const onInputChangeHandler = (event) =>{
        dispatch(inputChangeHandler(event.target.value));
    }
    return (
        <Container>
            <Grid container spacing={2} mt={5} mb={5} alignItems="center">
                <Grid item xs={12} md={2} lg={2} sm={2}>
                    <p><b>Nhập nội dung lọc</b></p>
                </Grid>
                <Grid item xs={12} md={8} lg={8} sm={8}>
                    <TextField fullWidth onChange={onInputChangeHandler} value={input}/>
                </Grid>
                <Grid item xs={12} md={2} lg={2} sm={2}>
                    <Button variant="outlined" onClick={onButtonFilterHandler}>Lọc</Button>
                </Grid>
            </Grid>
            <TableContainer component={Paper}>
                <Table>
                    <TableHead style={{backgroundColor:"brown"}}>
                        <TableRow container>
                            <TableCell style={{color:"white"}}>
                                STT
                            </TableCell >
                            <TableCell style={{color:"white"}}>
                                Nội dung
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {status === true?
                                <TableRow container>
                                    <TableCell>{1}</TableCell>
                                    <TableCell>{taskList}</TableCell>
                                </TableRow>
                         : Drink.map((value, index)=>{
                            return(
                            <TableRow container>
                                <TableCell key={index}>{index + 1}</TableCell>
                                <TableCell>{value.name}</TableCell>
                            </TableRow>
                            )})}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    )
}

export default Task;
